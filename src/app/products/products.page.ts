import { Component, Input, OnInit } from "@angular/core";
import { ProductService } from "./product.service";
import { hotel, lugares } from "./products.model";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";

@Component({
  selector: "page-products",
  templateUrl: "./products.page.html",
  styleUrls: ["./products.page.scss"],
})
export class ProductsPage implements OnInit {
  @Input() nombre: string;
  lugars: lugares[];
  constructor(
    private productServices: ProductService,
    private router: Router,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    console.log("Carga inicial");
    this.lugars = this.productServices.getAll();
  }

  ionViewWillEnter() {
    console.log("Se obtuvo la lista");
    this.lugars = this.productServices.getAll();
  }

  view(code: number) {
    this.router.navigate(["/products/detail/" + code]);
  }

  delete(code: number) {
    this.alertController
      .create({
        header: "Borrar lugar",
        message: "Esta seguro que desea borrar este lugar?",
        buttons: [
          {
            text: "No",
            role: "no",
          },
          {
            text: "Borrar",
            handler: () => {
              this.productServices.deleteLugar(code);
              this.lugars = this.productServices.getAll();
            },
          },
        ],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }

  update(code: number) {
    this.router.navigate(["/products/edit/" + code]);
  }

 

}

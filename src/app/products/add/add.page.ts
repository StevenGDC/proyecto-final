import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ProductService } from "../product.service";

@Component({
  selector: "app-add",
  templateUrl: "./add.page.html",
  styleUrls: ["./add.page.scss"],
})
export class AddPage implements OnInit {
  formProdutAdd: FormGroup;
  constructor(private serviceProduct: ProductService, private router: Router) {}

  ngOnInit() {

    this.formProdutAdd = new FormGroup({
      psitio: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pnombre: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pubicacion: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pprecio: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pfecha_disponibilidad: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pcodigo: new FormControl(1, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(1)],
      }),
      pdescripcion: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(20)],
      }),
    });
  }

  addLugar() {

    if (!this.formProdutAdd.valid) {
      return;
    }

    this.serviceProduct.addLugar(
      this.formProdutAdd.value.psitio,
      this.formProdutAdd.value.pnombre,
      this.formProdutAdd.value.pubicacion,
      this.formProdutAdd.value.pprecio,
      this.formProdutAdd.value.pfecha_disponibilidad,
      this.formProdutAdd.value.pcodigo,
      this.formProdutAdd.value.pdescripcion
    );

    this.formProdutAdd.reset();
    
    this.router.navigate(["/products"]);
    
  }
}

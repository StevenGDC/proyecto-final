
export interface lugares{
    sitio: string;
    nombre: string;
    ubicacion: string;
    precio: string;
    fecha_disponibilidad: string;
    descripcion: string;
    codigo: number;
    hotel: hotel[];
}
export interface hotel{
    cuartos: number;
    banos: number;
    cocina: string;
    sala: string;
    piscina:string;
    codigo: number;
}

//lugares
//sitio(string), nombre(string), ubicacion(string), precio(string), 
//fecha_disponibilidad(string), descripcion(string), codigo(number)

//hotel
//cuartos(number), baños(number), cocina(string), sala(string), piscina(string), codigo(number)



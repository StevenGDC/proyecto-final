import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ProductService } from "../product.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { lugares } from '../products.model';

@Component({
  selector: "app-edit",
  templateUrl: "./edit.page.html",
  styleUrls: ["./edit.page.scss"],
})
export class EditPage implements OnInit {
  lugar: lugares;
  formProductEdit: FormGroup;

  constructor(
    private activeRouter: ActivatedRoute,
    private serviceProduct: ProductService,
    private router: Router
  ) {}

  ngOnInit() {

    this.activeRouter.paramMap.subscribe(paramMap => {
      if (!paramMap.has('lugarId')) {
        return;
      }
      const lugarId = parseInt(paramMap.get('lugarId'));
      this.lugar = this.serviceProduct.getLugar(lugarId);

    });

    this.formProductEdit = new FormGroup({
      psitio: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pnombre: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pubicacion: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pprecio: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pfecha_disponibilidad: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pcodigo: new FormControl(1, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(1)],
      }),
      pdescripcion: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(20)],
      }),
    });

    this.formProductEdit.value.pnombre = this.lugar.nombre;

  }

  editLugar() {

    if (!this.formProductEdit.valid) {
      return;
    }

    this.serviceProduct.editLugar(
      this.formProductEdit.value.psitio,
      this.formProductEdit.value.pnombre,
      this.formProductEdit.value.pubicacion,
      this.formProductEdit.value.pprecio,
      this.formProductEdit.value.pfecha_disponibilidad,
      this.formProductEdit.value.pcodigo,
      this.formProductEdit.value.pdescripcion,

    );

    this.formProductEdit.reset();
    
    this.router.navigate(['./products']);
    
  }
}